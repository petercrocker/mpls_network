Vagrant.configure("2") do |config|
  config.vm.define "ext1" do |ext1|
    ext1.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-01"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "ext1"
    ext1.vm.box = "juniper/vsrx3"
    ext1.vm.guest = :tinycore
    ext1.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    ext1.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    ext1.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    ext1.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    ext1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.1.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.3.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_ext1_0",
                  auto_config: false
  end
  config.vm.define "ext2" do |ext2|
    ext2.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-02"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "ext2"
    ext2.vm.box = "juniper/vsrx3"
    ext2.vm.guest = :tinycore
    ext2.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    ext2.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    ext2.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    ext2.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    ext2.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.2.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.7.3",
                  :libvirt__tunnel_port => "10003",
                  :libvirt__iface_name => "vgif_ext2_0",
                  auto_config: false
  end
  config.vm.define "r1" do |r1|
    r1.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-03"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "r1"
    r1.vm.box = "juniper/vsrx3"
    r1.vm.guest = :tinycore
    r1.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    r1.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    r1.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    r1.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    r1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.3.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.1.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_r1_0",
                  auto_config: false
    r1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.3.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.4.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_r1_1",
                  auto_config: false
    r1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.3.2",
                  :libvirt__tunnel_local_port => "10002",
                  :libvirt__tunnel_ip => "127.1.10.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_r1_2",
                  auto_config: false
    r1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.3.3",
                  :libvirt__tunnel_local_port => "10003",
                  :libvirt__tunnel_ip => "127.1.11.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_r1_3",
                  auto_config: false
  end
  config.vm.define "r2" do |r2|
    r2.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-04"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "r2"
    r2.vm.box = "juniper/vsrx3"
    r2.vm.guest = :tinycore
    r2.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    r2.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    r2.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    r2.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    r2.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.4.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.3.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_r2_0",
                  auto_config: false
    r2.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.4.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.11.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_r2_1",
                  auto_config: false
  end
  config.vm.define "r3" do |r3|
    r3.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-05"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "r3"
    r3.vm.box = "juniper/vsrx3"
    r3.vm.guest = :tinycore
    r3.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    r3.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    r3.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    r3.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    r3.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.5.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.11.3",
                  :libvirt__tunnel_port => "10003",
                  :libvirt__iface_name => "vgif_r3_0",
                  auto_config: false
    r3.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.5.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.12.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_r3_1",
                  auto_config: false
  end
  config.vm.define "r4" do |r4|
    r4.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-06"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "r4"
    r4.vm.box = "juniper/vsrx3"
    r4.vm.guest = :tinycore
    r4.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    r4.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    r4.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    r4.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    r4.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.6.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.12.3",
                  :libvirt__tunnel_port => "10003",
                  :libvirt__iface_name => "vgif_r4_0",
                  auto_config: false
    r4.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.6.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.13.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_r4_1",
                  auto_config: false
    r4.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.6.2",
                  :libvirt__tunnel_local_port => "10002",
                  :libvirt__tunnel_ip => "127.1.7.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_r4_2",
                  auto_config: false
  end
  config.vm.define "r5" do |r5|
    r5.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-07"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "r5"
    r5.vm.box = "juniper/vsrx3"
    r5.vm.guest = :tinycore
    r5.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    r5.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    r5.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    r5.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    r5.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.7.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.6.2",
                  :libvirt__tunnel_port => "10002",
                  :libvirt__iface_name => "vgif_r5_0",
                  auto_config: false
    r5.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.7.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.8.2",
                  :libvirt__tunnel_port => "10002",
                  :libvirt__iface_name => "vgif_r5_1",
                  auto_config: false
    r5.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.7.2",
                  :libvirt__tunnel_local_port => "10002",
                  :libvirt__tunnel_ip => "127.1.13.3",
                  :libvirt__tunnel_port => "10003",
                  :libvirt__iface_name => "vgif_r5_2",
                  auto_config: false
    r5.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.7.3",
                  :libvirt__tunnel_local_port => "10003",
                  :libvirt__tunnel_ip => "127.1.2.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_r5_3",
                  auto_config: false
  end
  config.vm.define "r6" do |r6|
    r6.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-08"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "r6"
    r6.vm.box = "juniper/vsrx3"
    r6.vm.guest = :tinycore
    r6.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    r6.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    r6.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    r6.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    r6.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.8.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.12.4",
                  :libvirt__tunnel_port => "10004",
                  :libvirt__iface_name => "vgif_r6_0",
                  auto_config: false
    r6.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.8.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.13.2",
                  :libvirt__tunnel_port => "10002",
                  :libvirt__iface_name => "vgif_r6_1",
                  auto_config: false
    r6.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.8.2",
                  :libvirt__tunnel_local_port => "10002",
                  :libvirt__tunnel_ip => "127.1.7.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_r6_2",
                  auto_config: false
  end
  config.vm.define "r7" do |r7|
    r7.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-09"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "r7"
    r7.vm.box = "juniper/vsrx3"
    r7.vm.guest = :tinycore
    r7.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    r7.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    r7.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    r7.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    r7.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.9.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.11.4",
                  :libvirt__tunnel_port => "10004",
                  :libvirt__iface_name => "vgif_r7_0",
                  auto_config: false
    r7.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.9.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.12.2",
                  :libvirt__tunnel_port => "10002",
                  :libvirt__iface_name => "vgif_r7_1",
                  auto_config: false
  end
  config.vm.define "r8" do |r8|
    r8.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-0A"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "r8"
    r8.vm.box = "juniper/vsrx3"
    r8.vm.guest = :tinycore
    r8.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    r8.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    r8.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    r8.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    r8.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.10.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.3.2",
                  :libvirt__tunnel_port => "10002",
                  :libvirt__iface_name => "vgif_r8_0",
                  auto_config: false
    r8.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.10.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.11.2",
                  :libvirt__tunnel_port => "10002",
                  :libvirt__iface_name => "vgif_r8_1",
                  auto_config: false
  end
  config.vm.define "c1" do |c1|
    c1.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-0B"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "c1"
    c1.vm.box = "juniper/vsrx3"
    c1.vm.guest = :tinycore
    c1.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    c1.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    c1.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    c1.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    c1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.11.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.3.3",
                  :libvirt__tunnel_port => "10003",
                  :libvirt__iface_name => "vgif_c1_0",
                  auto_config: false
    c1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.11.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.4.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_c1_1",
                  auto_config: false
    c1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.11.2",
                  :libvirt__tunnel_local_port => "10002",
                  :libvirt__tunnel_ip => "127.1.10.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_c1_2",
                  auto_config: false
    c1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.11.3",
                  :libvirt__tunnel_local_port => "10003",
                  :libvirt__tunnel_ip => "127.1.5.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_c1_3",
                  auto_config: false
    c1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.11.4",
                  :libvirt__tunnel_local_port => "10004",
                  :libvirt__tunnel_ip => "127.1.9.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_c1_4",
                  auto_config: false
    c1.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.11.5",
                  :libvirt__tunnel_local_port => "10005",
                  :libvirt__tunnel_ip => "127.1.12.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_c1_5",
                  auto_config: false
  end
  config.vm.define "c2" do |c2|
    c2.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-0C"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "c2"
    c2.vm.box = "juniper/vsrx3"
    c2.vm.guest = :tinycore
    c2.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    c2.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    c2.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    c2.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    c2.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.12.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.11.5",
                  :libvirt__tunnel_port => "10005",
                  :libvirt__iface_name => "vgif_c2_0",
                  auto_config: false
    c2.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.12.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.5.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_c2_1",
                  auto_config: false
    c2.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.12.2",
                  :libvirt__tunnel_local_port => "10002",
                  :libvirt__tunnel_ip => "127.1.9.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_c2_2",
                  auto_config: false
    c2.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.12.3",
                  :libvirt__tunnel_local_port => "10003",
                  :libvirt__tunnel_ip => "127.1.6.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_c2_3",
                  auto_config: false
    c2.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.12.4",
                  :libvirt__tunnel_local_port => "10004",
                  :libvirt__tunnel_ip => "127.1.8.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_c2_4",
                  auto_config: false
    c2.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.12.5",
                  :libvirt__tunnel_local_port => "10005",
                  :libvirt__tunnel_ip => "127.1.13.0",
                  :libvirt__tunnel_port => "10000",
                  :libvirt__iface_name => "vgif_c2_5",
                  auto_config: false
  end
  config.vm.define "c3" do |c3|
    c3.vm.provider :libvirt do |domain|
      domain.management_network_mac = "08-4F-A9-00-00-0D"
      domain.qemu_use_session = false
    end
    cwd = Dir.pwd.split("/").last
    guest_name = "c3"
    c3.vm.box = "juniper/vsrx3"
    c3.vm.guest = :tinycore
    c3.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

    c3.ssh.insert_key = false

    # Limit CPU once Box has finished booting.
    c3.trigger.after :up do |trigger|
      trigger.name = "Finished Message"
      trigger.info = "Machine is up! : #{cwd}_#{guest_name}"
      trigger.run = {inline: "virsh schedinfo #{cwd}_#{guest_name} --set vcpu_quota=35000"}
    end

    c3.vm.provider :libvirt do |domain|
      domain.default_prefix = "#{cwd}_"
      domain.cpus = 2
      domain.memory = 4096
      domain.disk_bus = "ide"
      domain.cpu_mode = "custom"
    end
    c3.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.13.0",
                  :libvirt__tunnel_local_port => "10000",
                  :libvirt__tunnel_ip => "127.1.12.5",
                  :libvirt__tunnel_port => "10005",
                  :libvirt__iface_name => "vgif_c3_0",
                  auto_config: false
    c3.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.13.1",
                  :libvirt__tunnel_local_port => "10001",
                  :libvirt__tunnel_ip => "127.1.6.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_c3_1",
                  auto_config: false
    c3.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.13.2",
                  :libvirt__tunnel_local_port => "10002",
                  :libvirt__tunnel_ip => "127.1.8.1",
                  :libvirt__tunnel_port => "10001",
                  :libvirt__iface_name => "vgif_c3_2",
                  auto_config: false
    c3.vm.network :private_network,
                  :libvirt__tunnel_type => "udp",
                  :libvirt__tunnel_local_ip => "127.1.13.3",
                  :libvirt__tunnel_local_port => "10003",
                  :libvirt__tunnel_ip => "127.1.7.2",
                  :libvirt__tunnel_port => "10002",
                  :libvirt__iface_name => "vgif_c3_3",
                  auto_config: false
  end
end